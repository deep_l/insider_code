package com.app.musicsearch.listeners;

import android.widget.ImageView;

import com.app.musicsearch.models.MusicSearchResponse;

/**
 * Created by jay.l on 23/08/17.
 */

public interface ShowDetailedTrack {
    void selectedItem(MusicSearchResponse.Results result, ImageView artowrkImage);
}
