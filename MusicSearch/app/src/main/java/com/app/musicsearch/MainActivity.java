package com.app.musicsearch;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.musicsearch.adapters.SearchHintAdapter;
import com.app.musicsearch.adapters.TrackListingAdapter;
import com.app.musicsearch.database.DatabaseHelper;
import com.app.musicsearch.listeners.ShowDetailedTrack;
import com.app.musicsearch.models.MusicSearchResponse;
import com.app.musicsearch.network.ApiClient;
import com.app.musicsearch.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener, AdapterView.OnItemClickListener, ShowDetailedTrack {


    @BindView(R.id.music_content)
    RecyclerView musicContent;

    @BindView(R.id.search_music)
    AutoCompleteTextView searchMusic;

    @BindView(R.id.no_results_found)
    RelativeLayout noResultsView;

    private List<MusicSearchResponse.Results> trackContent;
    private TrackListingAdapter adapter;
    String[] array = {"first", "second item", "third item"};
    private ArrayList<String> queryData;
    private DatabaseHelper databaseHelper;
    SearchHintAdapter searchHintAdapter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        trackContent = new ArrayList<>();
        queryData = new ArrayList<>();
        adapter = new TrackListingAdapter(trackContent, getApplicationContext(), this);
        musicContent.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        musicContent.setAdapter(adapter);
        searchMusic.setOnTouchListener(this);
        searchMusic.setOnItemClickListener(this);
        musicContent.setOnTouchListener(this);
        databaseHelper = DatabaseHelper.getInstance(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.progress_message));

        //adapter for autocomplete textview
        searchHintAdapter = new SearchHintAdapter(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, queryData);

        searchMusic.setAdapter(searchHintAdapter);
        searchMusic.addTextChangedListener(queryListener);


        //listener for the search button on the keyboard
        searchMusic.setOnEditorActionListener(new EditText.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                searchMusic.setFocusable(true);
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!searchMusic.getText().toString().isEmpty()) {
                        String query = searchMusic.getText().toString();
                        getData(query);
                        databaseHelper.insertData(query);
                        queryData.add(query);
                        hideKeyboard();
                    }
                    return true;
                }
                return false;
            }

        });
    }

    //text watcher for autocomplete textview
    TextWatcher queryListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            queryData.clear();
            queryData.addAll(databaseHelper.getQueryData(charSequence.toString()));
            searchHintAdapter.updateData(queryData);
//                searchHintAdapter.notifyDataSetChanged();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };


    //custom listener for starting activity from adapter
    @Override
    public void selectedItem(MusicSearchResponse.Results result, ImageView artworkImage) {
        Intent startDetailedView = new Intent(this, DetailedTrackView.class);
        startDetailedView.putExtra("trackData", result);
        startDetailedView.putExtra("transition_image_name", ViewCompat.getTransitionName(artworkImage));

        //transition elements
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                artworkImage,
                ViewCompat.getTransitionName(artworkImage));
        startActivity(startDetailedView, options.toBundle());
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.search_music:
                //listener for the drawable-end button on the edittext
                final int DRAWABLE_RIGHT = 2;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (searchMusic.getRight() - searchMusic.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (!searchMusic.getText().toString().isEmpty()) {
                            String query = searchMusic.getText().toString();
                            getData(query);
                            databaseHelper.insertData(query);

                            hideKeyboard();
                        }
                        return true;
                    }
                }
                break;
            case R.id.music_content:
                hideKeyboard();
                break;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (!searchMusic.getText().toString().isEmpty()) {
            getData(searchMusic.getText().toString());  //autocompletetextview on item click
        }

    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    public void getData(String key) {

        if (!progressDialog.isShowing())
            progressDialog.show();
        ApiClient.getClient().create(ApiInterface.class).getArtistDetails(key).enqueue(new Callback<MusicSearchResponse>() {
            @Override
            public void onResponse(Call<MusicSearchResponse> call, Response<MusicSearchResponse> response) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.isSuccessful() && response.body() != null && response.body().resultData.size() > 0) {
                    //successful response and valid query
                    musicContent.setVisibility(View.VISIBLE);
                    noResultsView.setVisibility(View.GONE);

                    trackContent.addAll(response.body().resultData);
                    adapter.notifyDataSetChanged();
                } else {
                    //unsuccessful response or invalid query
                    trackContent.clear();
                    adapter.notifyDataSetChanged();
                    noResultsView.setVisibility(View.VISIBLE);
                    musicContent.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<MusicSearchResponse> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                //unsuccessful response or invalid query
                trackContent.clear();
                adapter.notifyDataSetChanged();
                noResultsView.setVisibility(View.VISIBLE);
                musicContent.setVisibility(View.GONE);
            }
        });
    }
}
