package com.app.musicsearch.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jay.l on 22/08/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 4;

    private static final String DATABASE_NAME = "insider";
    private static DatabaseHelper sInstance;

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    private static final String TABLE_NAME = "search_queries";

    private static final String KEY_ID = "id";
    private static final String KEY_QUERY = "query";

    private static final String CREATE_TABLE_QUERIES = "CREATE TABLE " + TABLE_NAME + " (" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_QUERY + " TEXT UNIQUE)";

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_QUERIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (sInstance == null)
            sInstance = new DatabaseHelper(context.getApplicationContext());
        return sInstance;
    }

    //retreiving artist names
    public ArrayList<String> getQueryData(String query) {
        ArrayList<String> queryData = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_QUERY + " LIKE '%" + query + "%'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                queryData.add(cursor.getString(cursor.getColumnIndex(KEY_QUERY)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return queryData;
    }

    private static final String TAG = "DB:";

    //inserting names
    public void insertData(String query) {
        try {
            SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_QUERY, query);
            sqLiteDatabase.insertOrThrow(TABLE_NAME, null, values);
        } catch (SQLiteConstraintException e) {
            Log.d(TAG, e.getMessage());// value already exists
        }
    }
}
