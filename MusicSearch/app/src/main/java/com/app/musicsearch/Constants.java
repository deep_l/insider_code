package com.app.musicsearch;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by jay.l on 21/08/17.
 */

public class Constants {


    public static final String SEARCH_URL = "https://itunes.apple.com/search";


    public static Typeface getMonserratRegularTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/montserratregular.ttf");
    }

    public static Typeface getOpensansSemiboldTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/opensans_semibold.ttf");
    }

    public static Typeface getOpensansRegularTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/opensansregular.ttf");
    }
}
