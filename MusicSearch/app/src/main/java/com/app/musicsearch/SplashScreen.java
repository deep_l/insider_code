package com.app.musicsearch;

/**
 * Created by jay.l on 24/08/17.
 */


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SplashScreen extends Activity {


    @BindView(R.id.splash_icon)
    ImageView main_icon;

    @BindView(R.id.splash_welcome)
    TextView message;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        ButterKnife.bind(this);
        message.setTypeface(Constants.getOpensansSemiboldTypeface(this));
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(main_icon, "alpha", 1f, .3f);
        fadeOut.setDuration(600);
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(main_icon, "alpha", .3f, 1f);
        fadeIn.setDuration(600);

        final AnimatorSet mAnimationSet = new AnimatorSet();

        mAnimationSet.play(fadeIn).after(fadeOut);

        mAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAnimationSet.start();
            }
        });
        mAnimationSet.start();


        new Handler().postDelayed(new Runnable() {

			/*
             * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                mAnimationSet.end();
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
                finish();
                // close this activity
            }
        }, 3500);
    }
}

