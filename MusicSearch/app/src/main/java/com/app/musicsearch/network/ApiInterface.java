package com.app.musicsearch.network;

/**
 * Created by jay.l on 21/08/17.
 */

import com.app.musicsearch.Constants;
import com.app.musicsearch.models.MusicSearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {
    @GET(Constants.SEARCH_URL)
    Call<MusicSearchResponse> getArtistDetails(@Query("term") String term);


}
