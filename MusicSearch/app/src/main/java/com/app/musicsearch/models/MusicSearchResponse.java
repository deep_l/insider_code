package com.app.musicsearch.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jay.l on 21/08/17.
 */

public class MusicSearchResponse {

    @SerializedName("results")
    public ArrayList<Results> resultData;

    public class Results implements Serializable {

        @SerializedName("artistName")
        private String artistName;

        @SerializedName("trackName")
        private String trackName;

        @SerializedName("primaryGenreName")
        private String genre;

        @SerializedName("trackTimeMillis")
        private long duration;

        @SerializedName("artworkUrl100")
        private String artWorkUrl;

        @SerializedName("trackPrice")
        private String trackPrice;

        @SerializedName("currency")
        private String currency;

        @SerializedName("previewUrl")
        private String previewAudioUrl;

        @SerializedName("collectionName")
        private String collectionName;

        @SerializedName("releaseDate")
        private String releaseDate;

        @SerializedName("collectionViewUrl")
        private String collectionViewUrll;

        @SerializedName("artistViewUrl")
        private String artistViewUrl;

        @SerializedName("trackViewUrl")
        private String trackPreviewUrl;

        public Results(String artistName, String trackName, String genre, long duration, String artWorkUrl, String trackPrice, String currency, String previewAudioUrl, String collectionName, String releaseDate, String collectionViewUrll, String artistViewUrl, String trackPreviewUrl) {
            this.artistName = artistName;
            this.trackName = trackName;
            this.genre = genre;
            this.duration = duration;
            this.artWorkUrl = artWorkUrl;
            this.trackPrice = trackPrice;
            this.currency = currency;
            this.previewAudioUrl = previewAudioUrl;
            this.collectionName = collectionName;
            this.releaseDate = releaseDate;
            this.collectionViewUrll = collectionViewUrll;
            this.artistViewUrl = artistViewUrl;
            this.trackPreviewUrl = trackPreviewUrl;
        }

        public String getArtistName() {
            return artistName;
        }

        public String getTrackName() {
            return trackName;
        }

        public String getGenre() {
            return genre;
        }

        public long getDuration() {
            return duration;
        }

        public String getArtWorkUrl() {
            return artWorkUrl;
        }

        public String getTrackPrice() {
            return trackPrice;
        }

        public String getCurrency() {
            return currency;
        }

        public String getPreviewAudioUrl() {
            return previewAudioUrl;
        }

        public String getCollectionName() {
            return collectionName;
        }

        public String getReleaseDate() {
            return releaseDate;
        }

        public String getCollectionViewUrll() {
            return collectionViewUrll;
        }

        public String getArtistViewUrl() {
            return artistViewUrl;
        }

        public String getTrackPreviewUrl() {
            return trackPreviewUrl;
        }
    }


}
