package com.app.musicsearch.adapters;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.musicsearch.Constants;
import com.app.musicsearch.R;
import com.app.musicsearch.listeners.ShowDetailedTrack;
import com.app.musicsearch.models.MusicSearchResponse;
import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jay.l on 21/08/17.
 */

public class TrackListingAdapter extends RecyclerView.Adapter<TrackListingAdapter.TrackViewHolder> {


    private List<MusicSearchResponse.Results> trackData;
    private Context context;

    ShowDetailedTrack detailedTrackInstace;

    public TrackListingAdapter(List<MusicSearchResponse.Results> trackData, Context context, ShowDetailedTrack detailedTrackInstace) {
        this.trackData = trackData;
        this.context = context;
        this.detailedTrackInstace = detailedTrackInstace;
    }

    @Override
    public TrackViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.track_listing_adapter_view, parent, false);
        return new TrackViewHolder(view);
    }

    public static String convertSecondsToMmSs(long seconds) {
        long s = seconds % 60;
        long m = (seconds / 60) % 60;

        return String.format(Locale.ENGLISH, "%02d:%02d", m, s);
    }

    @Override
    public void onBindViewHolder(TrackViewHolder holder, int position) {
        MusicSearchResponse.Results trackContent = trackData.get(position);
        Glide.with(context).load(trackContent.getArtWorkUrl()).into(holder.artworkImage);
        holder.trackName.setText(trackContent.getTrackName());
        holder.artistName.setText(trackContent.getArtistName());
        holder.trackGenre.setText(trackContent.getGenre());
        holder.trackPrice.setText(trackContent.getCurrency() + " " + trackContent.getTrackPrice());
        holder.trackDuration.setText(convertSecondsToMmSs(trackContent.getDuration()));

        ViewCompat.setTransitionName(holder.artworkImage, trackContent.getTrackName());

    }

    @Override
    public int getItemCount() {
        return trackData.size();
    }

    public class TrackViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.card_view)
        CardView parentLayout;

        @BindView(R.id.artwork_image)
        ImageView artworkImage;

        @BindView(R.id.track_name)
        TextView trackName;

        @BindView(R.id.artist_name)
        TextView artistName;

        @BindView(R.id.track_genre)
        TextView trackGenre;

        @BindView(R.id.track_duration)
        TextView trackDuration;

        @BindView(R.id.track_price)
        TextView trackPrice;

        public TrackViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            trackName.setTypeface(Constants.getOpensansSemiboldTypeface(context));
            artistName.setTypeface(Constants.getOpensansRegularTypeface(context));
            trackGenre.setTypeface(Constants.getOpensansRegularTypeface(context));
            trackPrice.setTypeface(Constants.getOpensansRegularTypeface(context));
            trackDuration.setTypeface(Constants.getOpensansRegularTypeface(context));
            parentLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            detailedTrackInstace.selectedItem(trackData.get(getAdapterPosition()), artworkImage);
        }
    }

}
