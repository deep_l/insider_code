package com.app.musicsearch;

import android.app.ProgressDialog;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.musicsearch.models.MusicSearchResponse;
import com.bumptech.glide.Glide;

import java.io.IOException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.text.Html.FROM_HTML_MODE_LEGACY;

public class DetailedTrackView extends AppCompatActivity implements View.OnClickListener {


    @BindView(R.id.artwork_image)
    ImageView imageView;

    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id.app_bar)
    AppBarLayout appBarLayout;

    @BindView(R.id.previous_screen)
    ImageView previousScreen;


    @BindView(R.id.track_name)
    TextView trackName;

    @BindView(R.id.artist_name)
    TextView artistName;

    @BindView(R.id.collection_name)
    TextView collectionName;

    @BindView(R.id.release_date)
    TextView releaseDate;

    @BindView(R.id.track_duration)
    TextView duration;

    @BindView(R.id.details_header)
    TextView detailsHeader;

    MusicSearchResponse.Results trackResults;
    private boolean playPause;
    private MediaPlayer mediaPlayer;
    private boolean intialStage = true;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_track_view);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getIntent().getExtras() != null)
            trackResults = (MusicSearchResponse.Results) getIntent().getSerializableExtra("trackData");

        init();
        getSupportActionBar().setTitle(null);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String imageTransitionName = getIntent().getExtras().getString("transition_image_name");
            imageView.setTransitionName(imageTransitionName);
        }
        Glide.with(this).load(trackResults.getArtWorkUrl()).into(imageView);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(trackResults.getTrackName());
                    previousScreen.setVisibility(View.VISIBLE);
                    isShow = true;
                } else if (isShow) {
                    previousScreen.setVisibility(View.GONE);
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!playPause) {
                    fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_action_playback_pause));

                    if (intialStage)
                        new StreamTrack()
                                .execute(trackResults.getPreviewAudioUrl());
                    else {
                        if (!mediaPlayer.isPlaying())
                            mediaPlayer.start();
                    }
                    playPause = true;
                } else {
                    fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_action_playback_play));
                    if (mediaPlayer.isPlaying())
                        mediaPlayer.pause();
                    playPause = false;
                }
            }

        });


    }

    public void init() {
        fab = (FloatingActionButton) findViewById(R.id.fab);
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        detailsHeader.setTypeface(Constants.getOpensansSemiboldTypeface(this));
        trackName.setTypeface(Constants.getOpensansSemiboldTypeface(this));
        artistName.setTypeface(Constants.getOpensansSemiboldTypeface(this));
        collectionName.setTypeface(Constants.getOpensansSemiboldTypeface(this));
        releaseDate.setTypeface(Constants.getOpensansSemiboldTypeface(this));
        duration.setTypeface(Constants.getOpensansSemiboldTypeface(this));
        trackName.setMovementMethod(LinkMovementMethod.getInstance());
        artistName.setMovementMethod(LinkMovementMethod.getInstance());
        collectionName.setMovementMethod(LinkMovementMethod.getInstance());
        previousScreen.setOnClickListener(this);
        trackName.setText(fromHtml(trackResults.getTrackPreviewUrl(), trackResults.getTrackName()));
        artistName.setText(fromHtml(trackResults.getArtistViewUrl(), trackResults.getArtistName()));
        collectionName.setText(fromHtml(trackResults.getCollectionViewUrll(), trackResults.getCollectionName()));

        releaseDate.setText(trackResults.getReleaseDate().substring(0, trackResults.getReleaseDate().indexOf("T")));
        duration.setText(convertSecondsToMmSs(trackResults.getDuration()));
    }

    public static String convertSecondsToMmSs(long seconds) {
        long s = seconds % 60;
        long m = (seconds / 60) % 60;

        return String.format(Locale.ENGLISH, "%02d:%02d", m, s);
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html, String title) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml("<a href=" + html + ">" + title + "</a> ", FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml("<a href=" + html + ">" + title + "</a> ");
        }
        return result;
    }

    private class StreamTrack extends AsyncTask<String, Void, Boolean> {
        private ProgressDialog progress;

        @Override
        protected Boolean doInBackground(String... params) {
            // TODO Auto-generated method stub
            Boolean prepared;
            try {

                mediaPlayer.setDataSource(params[0]);

                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        intialStage = true;
                        playPause = false;
                        fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_action_playback_play));
                        mediaPlayer.stop();
                        mediaPlayer.reset();
                    }
                });
                mediaPlayer.prepare();
                prepared = true;
            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                Log.d("IllegarArgument", e.getMessage());
                prepared = false;
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            }
            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (progress.isShowing()) {
                progress.cancel();
            }
            Log.d("Prepared", "//" + result);
            mediaPlayer.start();

            intialStage = false;
        }


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            if (progress == null)
                progress = new ProgressDialog(DetailedTrackView.this);
            if (progress.isShowing())
                progress.cancel();
            this.progress.setMessage("Buffering...");
            this.progress.show();


        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.previous_screen:
                finish();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
}
